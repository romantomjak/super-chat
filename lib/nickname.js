var prefixes = [
	"Black", "White", "Grey", "Pink", "Red", "Orange", "Yellow", "Green", "Blue",
	"Bald", "Barking", "Two-toed", "Southern", "Jungle", "Great", "Common", "Australian",
	"Super", "Urban", "Quick", "Wicked", "Tall", "Short", "One-Eyed"
];

var postfixes = [
	"the Red", "the Great", "the African", "the Arab", "the Black", "the Blind", "the Chief",
	"the Devil", "the Fat", "the Forkbeard", "the German", "the Hairy", "the Old"
]

var animals = [
	"Buffalo", "Chimpanzee", "Lion", "Fox", "Bat", "Whale", "Beaver", "Duck", "Pirate",
	"Jackal", "Lizard", "Llama", "Meerkat", "Monkey", "Mongoose", "Ostrich", "Rabbit",
	"Squirrel", "Salmon", "Seal", "Sheep", "Wolf", "Tarantula", "Turtle", "Mushroom"
]

function randomItemFromArray(arr) {
	return arr[Math.floor(Math.random() * arr.length)]
}

exports.random = function () {
	var use_prefix = Math.floor((Math.random() * 50) + 1) > 25 ? 1 : 0;

	if (use_prefix) {
		prefix = randomItemFromArray(prefixes);
		animal = randomItemFromArray(animals);

		return prefix + " " + animal;
	} else {
		postfix = randomItemFromArray(postfixes);
		animal = randomItemFromArray(animals);

		return animal + " " + postfix;
	}
}
