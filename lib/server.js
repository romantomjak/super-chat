var fs = require('fs'),
	path = require('path'),
	server = require('socket.io'),
	static = require('node-static'),
	nickname = require('./nickname'),
	port = process.env.PORT || 3000,
	app = require('http').createServer(handler);

app.listen(port, function () {
	console.log('Server listening on port %d', port);
});

// serve index.html
var file = new static.Server(path.join(__dirname, '..', 'public'));
function handler(req, res) {
	file.serve(req, res);
}

// handle events
var io = server.listen(app),
	users = {};

var MessageType = {
	Error: 'error',
	Message: 'message',
	Info: 'info'
};

io.sockets.on('connection', function (socket) {

	socket.on('register', function (user, fn) {
		if (! user) {
			user = nickname.random();
		}

		while(users.hasOwnProperty(user)) {
			user = nickname.random();
		}

		fn(user);

		users[user] = socket.user = user;
		socket.broadcast.emit('message', MessageType.Info, user + ' joined');

		console.log(Object.keys(users).length + ' clients connected');
	});

	socket.on('message', function (message) {
		socket.broadcast.emit('message', MessageType.Message, message, socket.user);
	});

	socket.on('error', function (e) {
		console.log('Error: ' + (e ? e.message : 'Unknown error occured'));
	})

	socket.on('disconnect', function () {
		if (! socket.user) {
			return;
		}

		delete users[socket.user];
		socket.broadcast.emit('message', MessageType.Info, socket.user + ' disconnected');

		console.log(Object.keys(users).length + ' clients connected');
	});
});
