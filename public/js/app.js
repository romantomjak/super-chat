var socket = io.connect();

var MessageType = {
	Error: 'error',
	Warning: 'warning',
	Message: 'message',
	Info: 'info',
	Success: 'success'
};

/*
 * Events
 */
socket.on('connect', function () {
	socket.emit('register', socket.user, function (name) {
		socket.user = name;
		message(MessageType.Info, 'Welcome! You are known as ' + name);
	});
});

socket.on('error', function (e) {
	message(MessageType.Error, e ? e : 'Unknown error occured');
});

socket.on('reconnect', function () {
	message(MessageType.Success, 'Reconnected to the server!');
});

socket.on('reconnecting', function () {
	message(MessageType.Warning, 'Attempting to re-connect to the server...');
});

socket.on('message', message);

/* 
 * Helpers
 */
function clear() {
	$('#message').val('').focus();
}

function zeroPrepend(number) {
	if (number < 10) {
		return '0' + number;
	} else {
		return number;
	}
}

/*
 * DOM Manipulation
 */
 function message(type, text, from) {
	var date = new Date(),
		chat = $('#chat'),
		line = $('<div class="line"/>'),
		meta = $('<span class="meta"/>'),
		timestamp = $('<span class="timestamp text-muted"/>'),
		message = $('<span class="message"/>');

	timestamp.text('[' + zeroPrepend(date.getHours()) + ':' + zeroPrepend(date.getMinutes()) + '] ');
	meta.append(timestamp);

	switch(type) {
		case MessageType.Message:
			meta.addClass('text-muted');
			var user = $('<span class="user"/>').text(from);
			if (from === socket.user) {
				user.addClass('me');
			}
			meta.append(user);
			message.text(text);
			break;
		case MessageType.Error:
			message.addClass('text-danger');
			message.text(text);
			break;
		case MessageType.Warning:
			message.addClass('text-warning');
			message.text(text);
			break;
		case MessageType.Success:
			message.addClass('text-success');
			message.text(text);
			break;
		case MessageType.Info:
		default:
			message.addClass('text-muted');
			message.text(text);
	}

	// construct html
	meta.append(' ');
	line.append(meta).append(message);
	chat.append(line);
	chat[0].scrollTop = chat[0].scrollHeight;
}

$(document).ready(function () {
	$(window).resize(function() {
		var chat = $('#chat');
		chat[0].scrollTop = chat[0].scrollHeight;
	});

	$('#controls').submit(function () {
		var text = $('#message').val();

		if (! socket.connected) {
			message(MessageType.Error, 'Cannot send the message. The connection to the server was lost.');
		} else if (text.length === 0) {
			return false;
		} else {
			message(MessageType.Message, text, socket.user)
			socket.emit(MessageType.Message, text);
			clear();
		}
		return false;
	});
});
