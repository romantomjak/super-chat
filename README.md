# Super chat
by [r00m](https://r00m.wordpress.com/). Tweet to your feedback to [@r00m](https://twitter.com/r00m)!

## About
How I explored socket.io by creating a simple chat app on top of NodeJS. This was built in one of Fun Fridays that my company organises.

## Screenshot
![Super chat screenshot on Imgur](http://i.imgur.com/nLezbux.png)

## Installation
This assumes that you already have installed NodeJS, if not, [install](https://nodejs.org) it first.

1. Go in to project directory
2. Run `npm install`
3. Run `node lib/server.js `
4. Open [localhost](http://localhost:3000) and start chatting!

## License
Super chat is available under the WTFPL license. See the LICENSE file for more information